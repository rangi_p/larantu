import numpy as np
from copy import deepcopy

import torch

# self-defined packages
from .config import config
from .game_objects import Game, Agent, Bomb
from .network import Head


class Node:
    
    def __init__(self, prior, parent):
        self.visit_count = 0
        self.total_value = 0
        self.prior = prior
        self.parent = parent
        self.children = {}
        
    def value(self):
        if self.visit_count == 0:
            return 0
        else:
            return self.total_value / self.visit_count
    
    def expanded(self):
        return len(self.children) > 0



class Tree:
    
    def __init__(self, game, training, net=None, pipe=None):
        self.root = Node(1, None)
        self.game = game
        self.training = training
        self.net = net
        self.pipe = pipe
        self.num_simulations = config.num_simulations
        self.c_puct = config.c_puct
        self.temperature = config.temperature
        
        # expand root node
        value = self.evaluate(self.root, self.game)
        self.root.visit_count += 1
        self.root.total_value += value[self.root.agent_num]
        assert len(self.root.children) > 0
    
    def search(self):
        # perform a search num_simulations times, each time exploring the tree
        # until finding a leaf node
        for i in range(self.num_simulations):
            self.search_once()
    
    def search_once(self):
        node = self.root
        game_copy = deepcopy(self.game)
        game_copy.record_history = False
        search_path = [node]
        while node.expanded():
            assert node.agent_num == game_copy.current_agent, \
                    (node.agent_num, game_copy.current_agent)
            if game_copy.agents[game_copy.current_agent].alive:
                if 4 in node.children.keys():
                    assert 4 in game_copy.possible_actions(game_copy.current_agent)
                action, node = self.select_child(node)
            else:
                # change children to reflect fact that agent died
                node.children = {-1: Node(1.0, node)}
                action, node = -1, node.children[-1]
            game_copy.take_action(action)
            search_path.append(node)
            
        values = self.evaluate(node, game_copy)
        self.update_tree(search_path, values)
        
    
    def evaluate(self, node, game):
        # check if node is a terminal state
        if game.terminal():
            values = [agent.reward for agent in game.agents]
        if game.agents[game.current_agent].alive:
            # get policy and value output of neural network
            rotate_reflect = (np.random.randint(4), np.random.randint(2))
            
            if self.training:
                # running in training mode with multiprocessing
                # send neural network input to evaluator
                self.pipe.send(game.nn_input())
                # wait to get output back from evaluator
                p, value = self.pipe.recv()
            
            else:
                # running in evaluation mode without multiprocessing
                # add initial dimension to front of input so batch_size = 1
                # convert to torch.Tensor
                nn_input = torch.Tensor(game.nn_input(rotate_reflect)[None])
                # pass to neural network
                p, value = self.net(nn_input)
                # convert output back to numpy array and float
                # unrotate and unreflect policy output
                p = p.detach().numpy()[0]
                value = value.detach().item()
                p = game.unrotate_unreflect(p, rotate_reflect)
            
            # clip logits to range [-88, 88] so that np.float32 input to exp gives valid output
            p = np.clip(p, -88, 88)
            # convert policy logits to probabilities but only for legal actions
            policy = {a: np.exp(p[a]) for a in game.possible_actions(game.current_agent)}
            policy_sum = sum(policy.values())
            
            # expand the node making sure to normalize the priors
            for action, prior in policy.items():
                node.children[action] = Node(prior / policy_sum, node)
            values = [value if i == game.current_agent else -value/(len(game.agents) - 1)
                    for i in range(len(game.agents))]
            assert sum(values) == 0
        else:
            # skip evaluating the neural network and set action to -1
            # return zero value since value should only be returned if game is terminal
            node.children[-1] = Node(1.0, node)
            values = [0]*len(game.agents)
        
        node.agent_num = game.current_agent
        return values
    
    def select_child(self, node):
        # calculate selection condition for each child and choose child with
        # maximum score
        _, action, child = max(
                (self.selection_condition(node, child), action, child) 
                for action, child in node.children.items())
        return action, child
    
    def selection_condition(self, parent, child):
        # selection using PUCT algorithm
        score = self.c_puct * child.prior
        score *= np.sqrt(parent.visit_count) / (1 + child.visit_count)
        return score + child.value()
    
    def update_tree(self, search_path, values):
        assert len(values) == len(self.game.agents)
        for node in search_path:
            node.visit_count += 1
            # add value only to nodes with same agent as the corresponding
            # entry in values
            node.total_value += values[node.agent_num]
            #config.logger.debug(f'is root: {node is self.root}, node value: {node.value()}, agent num for node: {node.agent_num}')
    
    def get_action(self, selection_type='proportional'):
        if selection_type == 'proportional':
            # take the visit counts to the power of 1/temperature then sample
            # the corresponding actions from these values
            assert len(self.root.children) > 0
            actions, visit_counts_power = np.array(list(zip(
                    *[(action, child.visit_count**(1/self.temperature)) 
                    for action, child in self.root.children.items()]
                )))
            visit_counts_power /= visit_counts_power.sum()
            return int(np.random.choice(actions, p=visit_counts_power))
        elif selection_type == 'greedy':
            # return the action of the child with the highest visit count
            assert len(self.root.children) > 0
            actions_visit_counts = [(action, child.visit_count) 
                    for action, child in self.root.children.items()]
            action, _ = max(actions_visit_counts, key=lambda x: x[1])
            return action
        else:
            raise RuntimeError(f'selection_type={selection_type}: should be \'proportional\' or \'greedy\'')
    
    def recycle(self, action_taken):
        # find the child of the root node corresponding to action_taken
        # make this node the new root
        self.root = self.root.children[action_taken]
        self.root.parent = None
        # expand the new root if necessary
        if not self.root.expanded():
            value = self.evaluate(self.root, self.game)
            self.root.visit_count += 1
            self.root.total_value += value


def game_copy_for_tree_search(game, active_agents):
    active_agent = game.agents[game.current_agent]
    game_copy = deepcopy(game)
    game_copy.max_steps = config.mini_game_length
    game_copy.step = 1
    game_copy.record_history = False
    keep_agent = [agent.n in active_agents for agent in game.agents]
    game_copy.agents = [agent 
            for agent, keep in zip(game_copy.agents, keep_agent) if keep]
    for agent in game_copy.agents:
        agent.reward = 0
    game_copy.current_agent = [a.n for a in game_copy.agents].index(active_agent.n)
    # reset action buffer of game copy so that only actions by 
    # active agents are still kept
    game_copy.action_buffer = [action 
            for action, keep in zip(game_copy.action_buffer, 
                    keep_agent[:len(game_copy.action_buffer)])
            if keep]
    return game_copy



