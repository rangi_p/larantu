import numpy as np

# extra packages
import networkx as nx

# self-defined packages
# assume game state in form of Game object
from .config import config
from .game_objects import Game, Agent, Bomb

class AgentDecision:

    def __init__(self):
        self.action_queue = []
        
        self.dxdy_to_action_str = config.dxdy_to_action_str
        self.action_str_to_dxdy = config.action_str_to_dxdy
        self.network_decision_radius = config.network_decision_radius
    
    
    def get_move(self, game, agent):
        self.game = game
        self.agent = agent
        
        # check whether the agent is alive
        # if not return DEAD as action
        if not agent.alive:
            return 'DEAD'
        
        # check whether there are other agents alive or not
        if self.game.num_active_agents() < 2:
            # don't have to worry about killing others, can just collect coins
            return self.collect_coins()
        
        else:
            # find out how many other agents are on connected tiles
            self.update_connection_graph()
            connected_agents = []
            agent_coord = (self.agent.x, self.agent.y)
            connected_tiles = nx.node_connected_component(self.graph, agent_coord)
            alive_agents = [agent for agent in self.game.agents if agent.alive]
            for other in alive_agents:
                other_coord = other.x, other.y
                if other.n != self.agent.n and other_coord in connected_tiles:
                    connected_agents.append(other)
            
            if len(connected_agents) == 0:
                # there are no connected agents
                
                # check if an action queue has already been calculated
                # if yes then return the next action and remove it from queue
                # provided that it is a safe action (doesn't move into explosion)
                if len(self.action_queue) > 0:
                    if self.action_queue[0] == 'BOMB':
                        if self.agent.bomb_available():
                            return self.action_queue.pop(0)
                        else:
                            return 'WAIT'
                    if self.action_is_safe(self.action_queue[0]):
                        return self.action_queue.pop(0)
                    else:
                        return 'WAIT'
                
                # construct a weighted graph which estimates how long
                # it will take to reach each tile
                weighted_graph = self.weighted_graph()
                
                distances = []
                for other in alive_agents:
                    target = (other.x, other.y)
                    if other is not self.agent and target in weighted_graph:
                        connected_to_target = nx.node_connected_component(
                                nx.Graph(weighted_graph), target)
                        for intermediate_tile in connected_tiles.intersection(
                                connected_to_target):
                            # distance from current position to intermediate tile 
                            # according to graph of connected tiles
                            dist_to_tile = nx.shortest_path_length(self.graph,
                                    source=agent_coord, target=intermediate_tile)
                            # distance from that tile to the target agent
                            # according to weighted graph
                            dist_from_tile = nx.dijkstra_path_length(weighted_graph, 
                                    source=intermediate_tile, target=target)
                            total_dist = dist_to_tile + dist_from_tile
                            distances.append((total_dist, intermediate_tile, target))
                
                if len(distances) == 0:
                    # no suitable paths could be found
                    # wait until bombs clear up
                    return 'WAIT'
                
                # select the other agent with the shortest distance according to graph
                _, intermediate_tile, target = min(distances, key=lambda x: x[0])
                
                # construct an action queue
                path_to_intermediate_tile = nx.shortest_path(self.graph,
                        source=agent_coord, target=intermediate_tile)
                path_to_target = nx.dijkstra_path(weighted_graph, 
                        source=intermediate_tile, target=target)
                path = path_to_intermediate_tile[:-1] + path_to_target
                self.action_queue = []
                
                # first add movements to tiles which are already connected
                for i in range(len(path) - 1):
                    current_tile = path[i]
                    next_tile = path[i+1]
                    if next_tile in connected_tiles:
                        dx = next_tile[0] - current_tile[0]
                        dy = next_tile[1] - current_tile[1]
                        self.action_queue.append(self.dxdy_to_action_str[(dx, dy)])
                    else:
                        bomb_drop_tile = current_tile
                        break
                
                # then add bomb drop
                self.action_queue.append('BOMB')
                
                # finished constructing action queue
                # return first action and remove from queue
                # first check that it is not a bad action
                # eg dropping a bomb when no bomb is available 
                # or moving into an explosion
                if self.action_queue[0] == 'BOMB':
                    if self.agent.bomb_available():
                        return self.action_queue.pop(0)
                    else:
                        return 'WAIT'
                        
                if self.action_is_safe(self.action_queue[0]):
                    return self.action_queue.pop(0)
                else:
                    return 'WAIT'
            
            else:
                # there is at least one connected agent
                
                # find distances to other agents and find closest
                distances_to_others = []
                for other in connected_agents:
                    dist = nx.shortest_path_length(self.graph, 
                            source=agent_coord, target=(other.x, other.y))
                    distances_to_others.append((dist, other))
                min_dist, closest_agent = min(distances_to_others, key=lambda x: x[0])
                
                # if the minimum distance to another agent is below a threshold
                # defer the decision to the neural network
                if min_dist < self.network_decision_radius:
                    return_str = ['network_decision', self.agent.n]
                    for dist, other in distances_to_others:
                        if dist < self.network_decision_radius:
                            # only flag other agent as active if it is closer 
                            # than threshold to active agent
                            return_str.append(other.n)
                    return ','.join(return_str)
                
                # check if in the path of a bomb
                bomb_path = np.zeros_like(self.game.arena)
                for agent in self.game.agents:
                    for bomb in agent.bombs:
                        bomb_path += bomb.explosion_area().astype(int)
                if bomb_path[self.agent.x, self.agent.y]:
                    graph_without_others = self.graph.copy()
                    for other in alive_agents:
                        if other is not self.agent:
                            graph_without_others.remove_node((other.x, other.y))
                    
                    safe_tiles = nx.node_connected_component(graph_without_others,
                            agent_coord)
                    safe_tiles = [(x,y) for x,y in safe_tiles if not bomb_path[x,y]]
                    
                    distances = []
                    for tile in safe_tiles:
                        dist = nx.shortest_path_length(graph_without_others, 
                                agent_coord, tile)
                        distances.append((dist, tile))
                    
                    if len(distances) == 0:
                        # no safe tile was found, probably agent will die
                        return 'WAIT'
                    
                    _, safe_tile = min(distances, key=lambda x: x[0])
                    
                    safe_tile_path = nx.shortest_path(graph_without_others, 
                            agent_coord, safe_tile)
                    dx = safe_tile_path[1][0] - safe_tile_path[0][0]
                    dy = safe_tile_path[1][1] - safe_tile_path[0][1]
                    
                    action = self.dxdy_to_action_str[(dx, dy)]
                    return action
                
                # move toward closest agent
                path_to_agent = nx.shortest_path(self.graph, 
                        source=agent_coord, target=(closest_agent.x, closest_agent.y))
                dx = path_to_agent[1][0] - path_to_agent[0][0]
                dy = path_to_agent[1][1] - path_to_agent[0][1]
                action = self.dxdy_to_action_str[(dx, dy)]
                
                if self.action_is_safe(action):
                    return action
                else:
                    return 'WAIT'
    
    
    def collect_coins(self):
        # not implemented
        return 'WAIT'
    
    
    def update_connection_graph(self):
        # use NetworkX graph to get information about connected paths and shortest paths
        self.graph = nx.Graph()
        # boolean array of open tiles
        # consider agents as open since we are trying to find a path between agents
        # consider bombs and explosions as open since they are dealt with earlier
        open_tiles = self.game.arena == 0
        tiles = [(x,y) for x in range(17) for y in range(17)]
        self.graph.add_nodes_from(tiles)
        for x,y in tiles:
            if x < 16 and open_tiles[x,y] and open_tiles[x+1,y]:
                self.graph.add_edge((x,y), (x+1,y))
            if y < 16 and open_tiles[x,y] and open_tiles[x,y+1]:
                self.graph.add_edge((x,y), (x,y+1))
        
    
    def weighted_graph(self):
        # graph is a weighted directed graph which has weights of 1 when moving
        # onto an empty tile and weights of 4 when moving onto a tile with a crate
        # tries to estimate how many rounds it takes to move from one tile to another
        graph = nx.DiGraph()
        tiles = [(x,y) for x in range(17) for y in range(17)]
        arena = self.game.arena
        not_wall = arena != -1
        graph.add_nodes_from(tiles)
        for x, y in tiles:
            if x < 16 and not_wall[x,y] and not_wall[x+1,y]:
                weight1 = 4 if arena[x+1,y] == 1 else 1
                weight2 = 4 if arena[x,y] == 1 else 1
                graph.add_edge((x,y), (x+1,y), weight=weight1)
                graph.add_edge((x+1,y), (x,y), weight=weight2)
            if y < 16 and not_wall[x,y] and not_wall[x,y+1]:
                weight1 = 4 if arena[x,y+1] == 1 else 1
                weight2 = 4 if arena[x,y] == 1 else 1
                graph.add_edge((x,y), (x,y+1), weight=weight1)
                graph.add_edge((x,y+1), (x,y), weight=weight2)
        # remove any tiles which are in the path of the agent's bombs
        for bomb in self.agent.bombs:
            explosion = bomb.explosion_area()
            explosion_tiles = [(x,y) for x in range(17) for y in range(17) 
                    if explosion[x,y]]
            graph.remove_nodes_from(explosion_tiles)
        return graph
    
    
    def action_is_safe(self, action):
        # check that action does not move agent into the path of an explosion
        # also exclude moves onto tiles that will have an explosion of another 
        # agent in them from a currently ticking bomb
        dx, dy = self.action_str_to_dxdy[action]
        new_x, new_y = self.agent.x + dx, self.agent.y + dy
        bomb_path = np.zeros_like(self.game.arena)
        for agent in self.game.agents:
            for bomb in agent.bombs:
                if agent is self.agent:
                    if bomb.countdown in [0, -1]:
                        # own bomb will explode next turn so add to bomb_path
                        bomb_path += bomb.explosion_area().astype(int)
                else:
                    # add all future explosion tiles of the other agents
                    if bomb.countdown != -2:
                        bomb_path += bomb.explosion_area().astype(int)
        return not bomb_path[new_x, new_y]








