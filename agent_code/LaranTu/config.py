import torch
import os
import logging

class Config:
    
    def __init__(self):
        
        # tree search parameters
        self.num_simulations = 50
        self.c_puct = 3.0
        self.temperature = 1.0 
        self.mini_game_length = 20
        self.root_dirichlet_alpha = 1.5
        self.root_exploration_fraction = 0.25
        
        # neural network parameters
        self.num_blocks = 2
        self.input_channels = 8
        self.hidden_layer_channels = 16
        self.field_of_view = 15
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        
        # action definitions
        self.actions = ['UP', 'DOWN', 'LEFT', 'RIGHT', 'BOMB', 'WAIT']
        
        # history buffer length
        self.game_buffer_max_length = 40000
        
        # distance from other agent below which neural network is used instead of heuristic
        self.network_decision_radius = 5
        
        # game setup
        self.corners = [(1, 1), (1, 15), (15, 1), (15, 15)]
        self.crate_density = 0.75
        
        # game parameters
        self.max_steps = 100
        
        # conversions between different representations of actions
        self.action_str_to_dxdy = {
                'UP': (0, -1),
                'DOWN': (0, 1),
                'LEFT': (-1, 0),
                'RIGHT': (1, 0),
                'BOMB': (0, 0),
                'WAIT': (0, 0)
            }
        
        self.dxdy_to_action_str = {
                (0, -1): 'UP',
                (0, 1): 'DOWN',
                (-1, 0): 'LEFT',
                (1, 0): 'RIGHT',
                (0, 0): 'WAIT'
            }
        
        self.dxdy_to_action_num = {
                (0, -1): 0,
                (0, 1): 1,
                (-1, 0): 2,
                (1, 0): 3,
                (0, 0): 5
            }
        
        # multiprocessing
        self.num_workers = 4
        self.nn_input_shape = (self.num_workers, self.input_channels, 
                self.field_of_view, self.field_of_view)
        
        # training of network
        self.batch_size = 64
        self.num_training_loops = 1
        self.network_save_interval = 1
        self.learn_rate = 0.2
        
        # loading of network during training
        self.load_network = False
        self.net_path = os.path.join(os.getcwd(), 'LaranTu', '0.pt')
        
        # printout of useful config values
        self.config_print_blacklist = ['actions', 'corners', 'crate_density', 
                'action_str_to_dxdy', 'dxdy_to_action_str', 'dxdy_to_action_num', 
                'config_print_blacklist', 'nn_input_shape', 'net_path', 'logger']
        
        # logging
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler('log', mode='w')
        self.logger.addHandler(handler)


config = Config()
