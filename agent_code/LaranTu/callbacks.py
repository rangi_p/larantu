import numpy as np
from time import time

import settings

# extra packages
import torch

# self-defined packages
from .config import config
from .agent_decision import AgentDecision
from .game_objects import Game, Agent
from .tree_search import Tree, game_copy_for_tree_search
from .network import Head


def setup(self):
    
    # initialize AgentDecision object
    self.decision = AgentDecision()
    
    # initialize neural network and load trained weights
    self.net = Head()
    checkpoint = torch.load('agent_code/LaranTu/1.pt', map_location='cpu')
    self.net.load_state_dict(checkpoint['net_state_dict'])
    self.net.eval()

def act(self):
    
    start_time = time()
    
    # initialize Game object with given game state information if first step
    if self.game_state["step"] == 1:
        self.game = Game()
        self.game.arena = self.game_state['arena']
        self.game.agents = []
        for x,y,n,b,s in [self.game_state['self']] + self.game_state['others']:
            agent = Agent(x,y,n)
            self.game.agents.append(agent)
    
    else:    
        # update Game object with new game state information
        self.game.arena = self.game_state['arena']
        
        # infer actions each agent took
        infered_actions = {}
        for x,y,n,b,s in [self.game_state['self']] + self.game_state['others']:
            # get the corresponding agent in Game object
            agent = [agent for agent in self.game.agents if agent.n == n][0]
            assert agent.alive, f'agent: {agent}'
            # infer actions of each agent
            dx = x - agent.x
            dy = y - agent.y
            infered_actions[n] = config.dxdy_to_action_num[(dx, dy)]
            if not b and sum(not bomb.exploded() for bomb in agent.bombs) == 0:
                # agent has dropped a bomb which has not yet been added to Game object
                assert infered_actions[n] == 5, infered_actions[n]
                infered_actions[n] = 4
        
        # take infered action to update the Game object to the current state
        for agent in self.game.agents:
            if agent.n in infered_actions.keys():
                self.game.take_action(infered_actions[agent.n])
            else:
                self.game.take_action(-1)
        
        # check that agents for which no action could be infered a actually dead
        for agent in self.game.agents:
            if agent.n not in infered_actions.keys():
                # no action could be infered, so the agent must be dead
                assert not agent.alive, f'agent: {agent}'
        
        # SANITY CHECK
        for agent in self.game.agents:
            if agent.n in infered_actions.keys():
                # check that resulting state of agent is same as given by game_state
                a = [a for a in [self.game_state['self']] + self.game_state['others'] if a[2] == agent.n][0]
                assert (agent.x, agent.y) == (a[0], a[1]), (agent.n, (agent.x, agent.y), (a[0], a[1]))
                assert int(agent.bomb_available()) == a[3], \
                        ([b for b in agent.bombs], int(agent.bomb_available()), self.game_state['bombs'], a[3])
            matching_bombs = [(b[0],b[1]) for bomb in agent.bombs for b in self.game_state['bombs']
                    if (bomb.x,bomb.y) == (b[0],b[1])]
            assert len(matching_bombs) == len([bomb for bomb in agent.bombs if not bomb.exploded()]), \
                    (matching_bombs, [bomb for bomb in agent.bombs if not bomb.exploded()])
    
    # get decision from AgentDecision object
    action_str = self.decision.get_move(self.game, self.game.agents[0])
    if action_str.startswith('network_decision'):
        # perform tree search using the neural network and continually update
        # the next action to be performed
        active_agents = action_str.split(',')[1:]
        mini_game = game_copy_for_tree_search(self.game, active_agents)
        tree = Tree(mini_game, training=False, net=self.net)
        while time() - start_time < settings.s.timeout:
            tree.search_once()
            action = tree.get_action(selection_type='greedy')
            self.next_action = config.actions[action]
    else:
        # decision given by heuristic
        # return immediately
        self.next_action = action_str
        return

def reward_update(self):
    # NOTE: this function was not used to train the agent
    # training was controlled by run_training.py located in the directory above this file
    pass

def learn(self):
    # NOTE: this function was not used to train the agent
    # training was controlled by run_training.py located in the directory above this file
    pass
