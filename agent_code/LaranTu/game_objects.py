import numpy as np
from copy import deepcopy

# extra packages
import networkx as nx

# self-defined packages
from .config import config


class Agent:
    
    def __init__(self, x, y, n):
        self.x = x
        self.y = y
        self.n = n
        self.alive = True
        self.bombs = []
        self.reward = 0.
    
    def __eq__(self, other):
        return self.n == other.n
    
    def __ne__(self, other):
        return self.n != other.n
    
    def __repr__(self):
        return str(self.__dict__)
    
    def drop_bomb(self):
        self.bombs.append(Bomb(self.x, self.y))
    
    def bomb_available(self):
        return all(bomb.exploded() for bomb in self.bombs)
    
    def take_action(self, action, obstacles):
        # first decrement any bombs and remove old ones
        expired_bombs = []
        for bomb in self.bombs:
            bomb.countdown -= 1
            if bomb.countdown == -3:
                # explosion has finished and bomb should be removed
                expired_bombs.append(bomb)
        for bomb in expired_bombs:
            self.bombs.remove(bomb)
        
        if action == -1:
            if self.alive:
                # agent has been killed but how they died hasn't been communicated
                # to game object
                # just set them as dead and don't worry about giving reward to
                # correct agent, this should only happen when interfacing with
                # game engine and updating Game object to match information
                # passed in form of game_state dict
                # in that case it shouldn't matter if the reward is not accurate
                self.alive = False
        # move if action is a movement
        # movements are (in order): 'UP', 'DOWN', 'LEFT', 'RIGHT'
        # if movement is blocked by obstacle, do nothing
        elif action in range(4):
            dx, dy = [(0, -1), (0, 1), (-1, 0), (1, 0)][action]
            if obstacles[self.x + dx, self.y + dy] == 0:
                self.x += dx
                self.y += dy
        # drop a bomb if action is 'BOMB'
        elif action == 4:
            assert self.bomb_available(), f'agent {self.n} bombs: {[bomb.__dict__ for bomb in self.bombs]}'
            #if not self.bomb_available():
            if False:
                print(f'ERROR: ACTION DROP BOMB SHOULD NOT HAVE BEEN CHOSEN')
                print(f'agent {self.n} bombs: {[bomb.__dict__ for bomb in self.bombs]}')
            else:
                self.drop_bomb()
        # if nothing is done, make sure chosen action was 'WAIT'
        else:
            assert action == 5, f'action: {action}'


class Bomb:
    
    def __init__(self, x, y, countdown=4):
        # countdown: 4 -> ... -> 0 while bomb, -1 -> -2 while explosion
        self.x = x 
        self.y = y
        self.countdown = countdown
    
    def __repr__(self):
        return str(self.__dict__)
    
    def exploded(self):
        return self.countdown < 0
    
    def explosion_area(self):
        explosion = np.zeros((17, 17), dtype=bool)
        if self.x % 2 == 0:
            assert self.y % 2 == 1, self.y
            # explosion will not extend up or down since a wall is in the way
            # should be limited to game area
            xmin = max(1, self.x-3)
            xmax = min(self.x+3, 15)
            explosion[xmin:xmax+1, self.y] = True
        else:
            # explosion goes up and down
            ymin = max(1, self.y-3)
            ymax = min(self.y+3, 15)
            explosion[self.x, ymin:ymax+1] = True
            if self.y % 2 == 1:
                # explosion can extend left and right as well
                xmin = max(1, self.x-3)
                xmax = min(self.x+3, 15)
                explosion[xmin:xmax+1, self.y] = True
        return explosion



class Game:
    
    def __init__(self, max_steps=config.max_steps):
        self.max_steps = max_steps
        self.step = 1
        
        # set up initial crates
        # arena is a numpy array
        self.arena = (np.random.rand(17, 17) < config.crate_density).astype(int)
        self.arena[:1, :] = -1
        self.arena[-1:,:] = -1
        self.arena[:, :1] = -1
        self.arena[:,-1:] = -1
        for x in range(17):
            for y in range(17):
                if (x+1)*(y+1) % 2 == 1:
                    self.arena[x,y] = -1
        
        # make sure that corners and surrounding tiles are free
        for (x,y) in config.corners:
            for (xx,yy) in [(x,y), (x-1,y), (x+1,y), (x,y-1), (x,y+1)]:
                if self.arena[xx,yy] == 1:
                    self.arena[xx,yy] = 0
        
        # place agents in corners randomly
        # information about bombs and explosions is in agent object
        corner_coordinates = np.random.permutation(config.corners)
        self.agents = [Agent(x, y, str(n)) 
                for n, (x, y) in enumerate(corner_coordinates)]
        self.current_agent = 0
        
        # setup obstacles array
        self.reset_obstacles()
        
        # action buffer to collect actions before updating state after an action
        # has been collected from each agent
        self.action_buffer = []
    
    def take_action(self, action):
        # add action to action_buffer
        # if agent is dead action should be -1
        if not self.agents[self.current_agent].alive:
            assert action == -1
        
        if action == 4:
            assert self.agents[self.current_agent].bomb_available()
        
        self.action_buffer.append(action)
        # check if the play queue is full, if yes then perform all actions collected
        if len(self.action_buffer) == len(self.agents):
            assert self.current_agent == len(self.agents) - 1
            
            self.take_step(self.action_buffer)
            self.action_buffer = []
            # reset current player to 0
            self.current_agent = 0
        else:
            # update current agent to play
            self.current_agent = (self.current_agent + 1) % len(self.agents)
    
    def take_step(self, actions):
        # perform agent actions in random order
        for i in np.random.permutation(len(self.agents)):
            # move agent or drop bomb or wait
            # previously chosen move may no longer be valid if another agent 
            # has moved into the desired tile, hence pass new obstacles array
            agent = self.agents[i]
            action = actions[i]
            agent.take_action(action, self.obstacles)
            # refresh the obstacles array for the next agent
            self.reset_obstacles()
        
        # kill any agents in the path of an explosion
        # remove crates in path of explosion
        for bomb_owner in self.agents:
            for bomb in bomb_owner.bombs:
                if bomb.exploded():
                    explosion = bomb.explosion_area()
                    for agent in [agent for agent in self.agents if agent.alive]:
                        if explosion[agent.x, agent.y]:
                            # agent is dead
                            agent.alive = False
                            # positive reward is given to agent whose bomb killed the other
                            bomb_owner.reward += 1.
                            # negative reward to agent who died
                            agent.reward -= 1.
                            if agent is bomb_owner:
                                # agent killed themself
                                # negative reward given
                                bomb_owner.reward -= 1.
                                # positive reward shared among the others
                                others = [other for other in self.agents 
                                        if other.alive and other is not bomb_owner]
                                for other in others:
                                    other.reward += 1. / len(others)
                    # remove crates
                    for x in range(17):
                        for y in range(17):
                            if explosion[x, y] and self.arena[x, y] == 1:
                                self.arena[x, y] = 0
        
        self.step += 1
        
    def reset_obstacles(self):
        # obstacles include walls, crates, other players and bombs
        # a value of 0 means the tile is free, otherwise not
        # walls and crates
        self.obstacles = (self.arena != 0).astype(int)
        # agents and bombs
        for agent in self.agents:
            if agent.alive:
                self.obstacles[agent.x, agent.y] += 1
            for bomb in agent.bombs:
                if not bomb.exploded():
                    self.obstacles[bomb.x, bomb.y] += 1
    
    def possible_actions(self, agent_num):
        agent = self.agents[agent_num]
        if not agent.alive:
            return [-1]
        actions = []
        # movements up, down, left and right
        for i, (dx, dy) in enumerate([(0, -1), (0, 1), (-1, 0), (1, 0)]):
            if self.obstacles[agent.x + dx, agent.y + dy] == 0:
                actions.append(i)
        # dropping a bomb
        if agent.bomb_available():
            actions.append(4)
        # waiting
        actions.append(5)
        return actions
    
    def terminal(self):
        if self.max_steps == config.max_steps:
            # playing full game
            # don't stop unless only one agent is alive or all are dead
            terminal = any((
                    self.step > self.max_steps,
                    self.num_active_agents() < 2
                ))
        elif self.max_steps == config.mini_game_length:
            # playing mini-game
            # stop when at least one agent is dead
            terminal = any((
                    self.step > self.max_steps,
                    self.num_active_agents() < len(self.agents)
                ))
        else:
            raise RuntimeError(f'self.max_steps ({self.max_steps}) should be \
{config.max_steps} (full game) or {config.mini_game_length} (mini-game)')
        return terminal
        
    def num_active_agents(self):
        return np.sum([agent.alive for agent in self.agents])
    
    def nn_input(self, rotate_reflect=None):
        '''
        Turn game_state arrays, tuples and ints into channels to be used
        as input into a convolutional neural network.
        Input is from the perspective of the active agent.
        All inputs are in the range [0, 1].
        If rotate_reflect tuple is given, will make a random rotation and 
        reflection of the output.
        Returns numpy array of shape (8, 15, 15).
        '''
        # remove border wall which has no information
        # have to adjust other coordinates down by 1
        arena = self.arena[1:-1, 1:-1]
        
        space = (arena == 0).astype(int)
        crates = (arena == 1).astype(int)
        
        active_location = np.zeros_like(arena)
        active = self.agents[self.current_agent]
        x, y = active.x - 1, active.y - 1
        active_location[x, y] += 1
        active_bomb = int(active.bomb_available()) * active_location
        
        others_location = np.zeros_like(arena)
        others_bomb = np.zeros_like(arena)            
        bombs = np.zeros_like(arena)
        explosions = np.zeros_like(arena, dtype=np.float64)
        # check that one agent is being excluded from this loop
        assert np.sum([agent == active for agent in self.agents]) == 1, \
                np.sum([agent == active for agent in self.agents])
        for agent in self.agents:
            x, y = agent.x - 1, agent.y - 1
            if agent != active and agent.alive:
                others_location[x, y] += 1
                others_bomb[x, y] += int(agent.bomb_available())
            # add active bombs to bombs
            for bomb in agent.bombs:
                x, y = bomb.x - 1, bomb.y - 1
                t = bomb.countdown
                if t in range(5):
                    # rescale countdown to count up and be in range (0, 1]
                    # assumes countdown: 4 -> 3 -> ... -> 0
                    bombs[x, y] += (5. - t) / 5.
                elif t in range(-2, 0):
                    # rescale explosions countdown to be in range [0, 1]
                    # assumes countdown: -1 -> -2
                    # remove border wall
                    e = bomb.explosion_area()
                    explosions += e.astype(int)[1:-1, 1:-1] * (t + 2) / 2
                else:
                    raise RuntimeError(
                            f'Bomb countdown t={t} is outside of valid range')
        
        # 
        
        channel_stack = np.stack((
                space,
                crates,
                active_location,
                active_bomb,
                others_location,
                others_bomb,
                bombs,
                explosions
            ))
        
        if rotate_reflect:
            # rotate and reflect arrays randomly
            rotation, reflection = rotate_reflect
            assert rotation in range(4), rotation
            assert reflection in range(2), reflection
            channel_stack = np.rot90(channel_stack, k=rotation, axes=(1,2))
            if reflection: channel_stack = np.flip(channel_stack, axis=-1)
        
        assert channel_stack.shape == (config.input_channels, config.field_of_view, config.field_of_view)
        return channel_stack.copy()
    
    
    def unrotate_unreflect(self, policy, rotate_reflect):
        # perform reverse transformation on network output of the transformation
        # that was done on the input to give a valid action
        rotation, reflection = rotate_reflect
        unreflect = np.arange(4)
        if reflection:
            unreflect[0] = 1
            unreflect[1] = 0
        unrotate = np.array([3, 2, 0, 1])
        
        transformed = policy[:4]
        transformed = transformed[unreflect]
        for i in range(rotation):
            transformed = transformed[unrotate]
        policy[:4] = transformed
        
        return policy





