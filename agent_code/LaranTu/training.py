import numpy as np
from copy import deepcopy
import os
import queue
import pickle
import yaml

# extra packages
import torch
import torch.nn as nn
import torch.nn.functional as F

# self-defined packages
from .config import config
from .game_objects import Game, Agent, Bomb
from .agent_decision import AgentDecision
from .network import Head
from .tree_search import Tree, game_copy_for_tree_search


def play_game(self_play_queue, pipe):
    # set up new game and network
    
    np.random.seed()
    
    while True:
    
        game = Game()
        # make an agent decision object for each agent so a queue of next actions can be stored in it
        agent_decision = [AgentDecision() for i in range(4)]
        
        print(f'process {os.getpid()}: starting new game (max_steps={game.max_steps})')
        
        while not game.terminal():
            agent = game.agents[game.current_agent]
            
            action_str = agent_decision[game.current_agent].get_move(game, agent)
            
            if action_str.startswith('network_decision'):
                # use current game state as starting position for a mini-game
                # use returned first action from mini-game as the action to take
                active_agents = action_str.split(',')[1:]
                action = play_mini_game(game, active_agents, self_play_queue, pipe)
            elif action_str == 'DEAD':
                action = -1
            else:
                action = config.actions.index(action_str)
            
            game.take_action(action)
        
        print(f'process {os.getpid()}: game over')


def play_mini_game(starting_game, active_agents, self_play_queue, pipe):
    
    mini_game = game_copy_for_tree_search(starting_game, active_agents)
    history = []
    tree = Tree(mini_game, training=True, pipe=pipe)
    first_action = None
    
    while not mini_game.terminal():
        tree.search()
        action = tree.get_action()
        visit_counts = {action: child.visit_count 
                for action, child in tree.root.children.items()}
        if first_action is None:
            # save first action as the action to be returned to main game
            first_action = action
            assert first_action in visit_counts.keys()
        nn_input = mini_game.nn_input()
        # save input to neural net along with info for pseudo ground truth to history buffer
        # these will be used as training examples
        history.append((nn_input, visit_counts, mini_game.current_agent))
        tree.recycle(action)
        mini_game.take_action(action)
    
    # put contents of history buffer onto self_play_queue which will be used as training examples
    for nn_input, visit_counts, current_agent in history:
        value = mini_game.agents[current_agent].reward
        self_play_queue.put((nn_input, visit_counts, value))
    
    return first_action


def evaluate_and_update_network(self_play_queue, pipes, now_str):
    
    # make new directories to store results
    # this function should be called by a script one directory above
    network_save_path = os.path.join(os.getcwd(), 'LaranTu', 'save', now_str, 'network_save')
    os.makedirs(network_save_path)
    game_buffer_save_path = os.path.join(os.getcwd(), 'LaranTu', 'save', now_str, 'game_buffer_save')
    os.makedirs(game_buffer_save_path)
    
    # save config info to file
    config_save_path = os.path.join(os.getcwd(), 'LaranTu', 'save', now_str, 'config.yaml')
    dict_to_save = {key: value for key, value in config.__dict__.items() 
            if key not in config.config_print_blacklist}
    if config.load_network:
        dict_to_save['net_path'] = config.net_path
    with open(config_save_path, 'w') as f:
        f.write(yaml.dump(dict_to_save, default_flow_style=False))
    
    # initialize network and optimizer and load to GPU if available
    net = Head().to(config.device)
    optimizer = torch.optim.SGD(net.parameters(), lr=config.learn_rate, momentum=0.9, weight_decay=1e-4)
    
    # load a previously saved checkpoint if wanted
    if config.load_network:
        print(f'loading network from {config.net_path}')
        if config.device == 'cuda':
            checkpoint = torch.load(config.net_path)
        else:
            checkpoint = torch.load(config.net_path, map_location='cpu')
        net.load_state_dict(checkpoint['net_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    
    # make initial save of network and optimizer
    net_path = os.path.join(network_save_path, '0.pt')
    print(f'saving network to {net_path}')
    torch.save(
            {
                    'net_state_dict': net.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict()
                }, 
            net_path
        )
    
    batch_size = config.batch_size
    game_buffer = []
    
    evaluator_input = np.empty(config.nn_input_shape)
    
    loss_total = 0.
    
    for n in range(config.num_training_loops):
        print(f'training loop {n}')
        
        # unload from self_play_queue
        net.eval()
        added_to_buffer = 0
        while added_to_buffer < batch_size // 8:
            # try to add a self play example from the self play queue
            try:
                self_play_example = self_play_queue.get(block=False)
                #config.logger.debug(f'self_play_example has value {self_play_example[2]}')
                if self_play_example[2] != 0:
                    game_buffer.append(self_play_example)
                    added_to_buffer += 1
                    if len(game_buffer) > config.game_buffer_max_length:
                        game_buffer.pop(0)
            except queue.Empty:
                pass
            
            # after example is loaded or no example found, evaluate the inputs to neural net
            # from each of the worker processes
            
            # get inputs to neural network
            for i in range(config.num_workers):
                evaluator_input[i] = pipes[i].recv()
            
            # get output of neural network and convert to numpy array
            policy, value = net(torch.Tensor(evaluator_input).to(config.device))
            policy = policy.detach().cpu().numpy()
            value = value.detach().cpu().numpy()
            
            # send results back to worker processes
            for i in range(config.num_workers):
                pipes[i].send((policy[i], value[i]))
        
        # enough examples have been unloaded from self play queue to do an update iteration of network
        
        # prepare inputs and targets
        nn_input = []
        target_policy = []
        target_value = []
        rr_save = []
        
        # get random uniform samples from the game_buffer containing self play examples and pseudo ground truths
        for i in np.random.randint(len(game_buffer), size=batch_size):
            nn_in, visit_counts, z = game_buffer[i]
            
            # make a random rotation and reflection of input to augment training data
            rotate_reflect = (np.random.randint(4), np.random.randint(2))
            rr_save.append(rotate_reflect)
            nn_in = np.rot90(nn_in, k=rotate_reflect[0], axes=(1,2))
            if rotate_reflect[1]: nn_in = np.flip(nn_in, axis=-1)
            nn_input.append(nn_in)
            
            # get target policy by exponentiating and normalizing visit counts from self play tree search
            visit_counts_power = np.array([visit_counts[a]**(1/config.temperature)
                    if a in visit_counts.keys() else 1e-6 for a in range(6)])
            visit_counts_power = visit_counts_power / np.sum(visit_counts_power)
            target_policy.append(visit_counts_power)
            
            target_value.append(z)
        
        # convert arrays to torch tensors
        nn_input = torch.Tensor(nn_input).to(config.device)
        target_policy = torch.Tensor(target_policy).to(config.device)
        target_value = torch.Tensor(target_value).to(config.device)
        
        # set net in training mode and get output
        net.train()
        policy, value = net(nn_input)
        
        config.logger.debug(f'target value: {target_value}')
        config.logger.debug(f'predicted value: {value}')
        config.logger.debug(f'target policy: {target_policy}')
        config.logger.debug(f'predicted policy: {torch.softmax(policy, 1)}')
        
        # unrotate and unreflect the policy to get the correct actions
        for i in range(batch_size):
            policy[i] = unrotate_unreflect(policy[i], rr_save[i])
        
        # reset optimizer gradient and calculate loss
        optimizer.zero_grad()
        loss = F.mse_loss(value, target_value)
        loss -= torch.einsum('nd,nd->n', F.log_softmax(policy, dim=1), 
                    target_policy).sum() / batch_size
        
        # find gradients and update weights
        loss.backward()
        optimizer.step()
        
        # keep running total of loss
        loss_total += loss.detach().item()
        
        if (n+1) % config.network_save_interval == 0:
            # make a checkpoint
            # save the network weights and optimizer state and average loss
            net_path = os.path.join(network_save_path, f'{n+1}.pt')
            print(f'saving network to {net_path}')
            print(f'average loss: {loss_total / config.network_save_interval}')
            torch.save(
                    {
                            'net_state_dict': net.state_dict(),
                            'optimizer_state_dict': optimizer.state_dict(),
                            'loss_avg': loss_total / config.network_save_interval
                        }, 
                    net_path
                )
            # save the game buffer
            game_buffer_increase = (batch_size // 8) * config.network_save_interval
            buffer_path = os.path.join(game_buffer_save_path, f'{n+1}.p')
            with open(buffer_path, 'wb') as f:
                pickle.dump(game_buffer[-game_buffer_increase:], f)
            
            # reset running total of loss
            loss_total = 0.


def unrotate_unreflect(policy, rotate_reflect):
    # perform reverse transformation on network output of the transformation that was done on the input 
    # this will give a valid action in the unrotated unreflected reference frame
    rotation, reflection = rotate_reflect
    unreflect = np.arange(4)
    if reflection:
        unreflect[0] = 1
        unreflect[1] = 0
    unrotate = np.array([3, 2, 0, 1])
    
    transformed = policy[:4]
    transformed = transformed[unreflect]
    for i in range(rotation):
        transformed = transformed[unrotate]
    policy[:4] = transformed
    
    return policy

