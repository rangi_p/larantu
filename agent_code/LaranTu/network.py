import numpy as np

# extra packages
import torch
import torch.nn as nn
import torch.nn.functional as F

# self-defined packages
from .config import config

# definition of neural network

def reshape_policy(x):
    return x.view(-1, config.hidden_layer_channels*config.field_of_view**2)

def reshape_value(x):
    return x.view(-1, config.field_of_view**2)

class Lambda(nn.Module):
    
    def __init__(self, func):
        super().__init__()
        self.func = func

    
    def forward(self, x):
        return self.func(x)
    


class Body(nn.Module):
    
    def __init__(self, channels):
        super().__init__()
        
        self.num_blocks = config.num_blocks
        assert self.num_blocks in [1,2,3,4], self.num_blocks
        
        self.first_conv = nn.Sequential(
                nn.Conv2d(config.input_channels, channels, 3, padding=1, bias=False),
                nn.BatchNorm2d(channels),
                nn.ReLU()
            )
        
        self.block1 = self.make_block(channels)
        if self.num_blocks > 1:
            self.block2 = self.make_block(channels)
        if self.num_blocks > 2:
            self.block3 = self.make_block(channels)
        if self.num_blocks > 3:
            self.block4 = self.make_block(channels)
        
    
    def forward(self, x):
        x = self.first_conv(x)
        x = F.relu(x + self.block1(x))
        if self.num_blocks > 1:
            x = F.relu(x + self.block2(x))
        if self.num_blocks > 2:
            x = F.relu(x + self.block3(x))
        if self.num_blocks > 3:
            x = F.relu(x + self.block4(x))
        return x
    
    
    def make_block(self, channels):
        return nn.Sequential(
                nn.Conv2d(channels, channels, 3, padding=1, bias=False),
                nn.BatchNorm2d(channels),
                nn.ReLU(),
                nn.Conv2d(channels, channels, 3, padding=1, bias=False),
                nn.BatchNorm2d(channels)
            )



class Head(nn.Module):
    
    def __init__(self, channels=config.hidden_layer_channels, 
            field_of_view=config.field_of_view):
        super().__init__()
        
        self.body = Body(channels)
        
        self.policy = nn.Sequential(
                nn.Conv2d(channels, channels, 1, padding=0, bias=False),
                nn.BatchNorm2d(channels),
                nn.ReLU(),
                Lambda(reshape_policy),
                nn.Linear(channels*field_of_view**2, 6)
            )
        
        self.value = nn.Sequential(
                nn.Conv2d(channels, 1, 1, padding=0, bias=False),
                nn.BatchNorm2d(1),
                nn.ReLU(),
                Lambda(reshape_value),
                nn.Linear(field_of_view**2, channels),
                nn.ReLU(),
                nn.Linear(channels, 1),
                nn.Tanh()
            )
    
    
    def forward(self, x):
        body_out = self.body(x)
        return self.policy(body_out), self.value(body_out)
    
        






