# NOTE: this script should be run one directory above the rest of the python files

import torch.multiprocessing as mp
from time import time
from datetime import datetime
import os

from LaranTu.training import play_game, evaluate_and_update_network
from LaranTu.config import config

import torch

if __name__ == '__main__':
    
    # get current time for use as timestamp
    now_str = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    start_time = time()
    
    # set up self play queue for worker processes to add play examples to
    mp.set_start_method('spawn')
    manager = mp.Manager()
    self_play_queue = manager.Queue()
    
    # set up a pipe for each worker to communicate with evaluator
    pipes = [mp.Pipe() for n in range(config.num_workers)]
    eval_pipes, worker_pipes = zip(*pipes)
    
    print(f'creating evaluator/updater to pass batches through neural network and update weights')
    evaluator = mp.Process(target=evaluate_and_update_network, 
            args=(self_play_queue, eval_pipes, now_str))
    evaluator.start()
    
    print(f'creating {config.num_workers} workers to generate self-play games')
    self_play_workers = [mp.Process(target=play_game, 
            args=(self_play_queue, worker_pipes[n]))
            for n in range(config.num_workers)]
    for p in self_play_workers:
        p.start()
    
    # don't move beyond here until the evaluator/updater has finished all its loops
    evaluator.join()
    
    # then terminate the other processes to end training
    for p in self_play_workers:
        p.terminate()
    
    runtime = time() - start_time
    print(f'total runtime: {runtime} s')
    config_save_path = os.path.join(os.getcwd(), 'LaranTu', 'save', now_str, 'config.yaml')
    with open(config_save_path, 'a') as f:
        f.write('\n' + f'runtime: {runtime} s')


